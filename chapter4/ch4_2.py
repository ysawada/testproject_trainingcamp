#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml


def hydromodel(Smax,Sfc,a,M,P,Ep,S,Q,Tend):
    for t in range(0,Tend):
        # Calculating fluxes
        Eb = Ep[t]*(1-M)*S[t]/Smax # Evaporation from soil
        if S[t] > Sfc:
            Ev = M*Ep[t] # transpiration from vegetation
            Qss = a*(S[t]-Sfc) # Subsurface runoff
        else:
            Ev = M*Ep[t]*S[t]/Sfc # transpiration from vegetation
            Qss = 0.0 # subsurface runoff
        if S[t] > Smax:
            Qse = P[t] # saturation excess overland flow
        else:
            Qse = 0.0 # no saturation excess overland flow
        Q[t] = Qss + Qse # total runoff
        
        # Updating Storage
        S[t+1] = S[t] + P[t] - Eb - Ev - Qse - Qss

        # Monitor
        #print('integrating at time', t)
        #print('rainfall, storage, runoff = ', P[t], S[t], Q[t])

    return S, Q

def NSE(f,o): #Nash-Sutcliffe Model Efficiency Coefficient
    clim = np.ones((len(o)))*np.nanmean(o)
    numerator = np.nansum((f-o)**2)
    denominator = np.nansum((o-clim)**2)
    return 1 - (numerator/denominator)

# model parameters
Smax_min=1.0; Smax_max=2000.0
Sfc_fraction_min=0.05; Sfc_fraction_max=0.95
a_min=0.0; a_max=1.0
M_min=0.05; M_max=0.95

# Other Configuration
Tend = 4017 # Total timesteps [d]
inc = 10 # the number of combinations for each parameter

# Initializing state variables
S = np.zeros((Tend+1)) #Storage [mm]
S[0] = 100.0


# reading input variables from MERV-Jp
filename = '../varssim086.csv'
data = np.loadtxt(filename,delimiter=',',skiprows=1)
P = data[0:4018,4] #precipitation [mm/d]
Ep = data[0:4018,6] #potential evapotranspiration [mm/d]

# output
Q = np.zeros((Tend))
NSEsim=np.zeros((inc,inc,inc,inc))

# observed runoff
obsQ = data[0:4018,7]
obsQ[obsQ<0] = nan

# initializing main exacutable code for the model
for i in range(0,inc): #note that minimum parameters are unstable, so we avoid it.
    Smax = Smax_min + (i+1) * (Smax_max-Smax_min)/inc
    for j in range(0,inc):
        Sfc = Smax * (Sfc_fraction_min + (j+1) * (Sfc_fraction_max-Sfc_fraction_min)/inc)
        for k in range(0,inc):
            a = a_min + (k+1) * (a_max-a_min)/inc
            for l in range(0,inc):
                M = M_min + (l+1) * (M_max-M_min)/inc
                S = np.zeros((Tend+1))
                S[0] = Smax/10.0
                Q = np.zeros((Tend))
                S, Q = hydromodel(Smax, Sfc, a, M, P, Ep, S, Q, Tend)
                NSEsim[i,j,k,l] = NSE(Q[0:Tend], obsQ[0:Tend])
                print('integrating at setting, ', i,j,k,l)
                print('parameters', Smax, Sfc, a, M)
                print('runoff', Q)
                print('NSE = ', NSEsim[i,j,k,l])

np.savetxt('output.txt', NSEsim.ravel())


