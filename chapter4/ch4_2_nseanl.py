#--------------------------------------------------------------
#
# Analyzing output.txt from ch4_1.f90
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml

inc = 10
filename = 'output.txt'
data = np.loadtxt(filename)

nse = data.reshape(inc,inc,inc,inc)
optpara = np.unravel_index(np.argmax(nse),nse.shape)
print(optpara)

Smax = 1.0 + (optpara[0]+1) * (2000-1)/inc
Sfc_fraction = 0.05 + (optpara[1]+1) * (0.95-0.05)/inc
a = 0 + (optpara[2]+1) * 1.0/inc
M = 0.05 + (optpara[3]+1) * (0.95-0.05)/inc

print('optimal Smax, Sfc, a, M = ', Smax, Sfc_fraction, a, M)
