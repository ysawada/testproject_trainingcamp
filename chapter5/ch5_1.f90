!
!Test code for MPI parallel programing
!
!Developed by Yohei Sawada
!
!in this version, parameters are hard-coded
!
program TestMPI
use mpi
implicit none


! for MPI
integer:: PETOT, my_rank, ierr
integer, allocatable:: output(:)
integer:: output_process

call MPI_INIT(ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, PETOT, ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierr)

allocate(output(PETOT)) ! size is identical to the total number of processes
output(my_rank+1) = my_rank**2
output_process = output(my_rank+1)
print *, "hello, world!", my_rank, output_process

call MPI_Gather(output_process, 1, MPI_INTEGER, output(my_rank+1), 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

!
! Writing output Q(t)
!
if (my_rank == 0) then
 open (21, file='output.txt',status='unknown')
 write(21,*) output
endif

call MPI_FINALIZE (ierr)

end program


